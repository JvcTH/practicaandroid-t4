package com.example.examen_final;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class crearPokemonActivity extends AppCompatActivity {

    ImageView imagePOKE;
    EditText nombrePoke;
    EditText tipoPoke;
    EditText latitudePoke;
    EditText longitudPoke;

    Button button2;
    Button registrarPoki;

    Uri imageUri;
    String imagenString;

    private static final int PICK_IMAGE = 11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_pokemon);

        imagePOKE = findViewById(R.id.imagePOKE);
        nombrePoke = findViewById(R.id.nombrePoke);
        tipoPoke = findViewById(R.id.tipoPoke);
        latitudePoke = findViewById(R.id.latitudePoke);
        longitudPoke = findViewById(R.id.longitudPoke);
        button2 = findViewById(R.id.button2);
        registrarPoki = findViewById(R.id.registrarPoki);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://upn.lumenes.tk/pokemons/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        servicio service = retrofit.create(servicio.class);

        registrarPoki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = nombrePoke.getEditableText().toString().trim();
                String tipo = tipoPoke.getEditableText().toString().trim();
                String lat = latitudePoke.getEditableText().toString().trim();
                String lon = longitudPoke.getEditableText().toString().trim();


                pokemon pok = new pokemon(nombre, tipo, imagenString, Float.parseFloat(lat), Float.parseFloat(lon));
                Call<Void> entre = service.POSTcrear(pok);
                entre.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        String respuesta = String.valueOf(response.code());
                        if (respuesta.equals("200")) {
                            Toast.makeText(getApplicationContext(), "Entrenador Registrado", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), "NO Entrenador Registrado", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {

                    }
                });
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarImagen();
            }
        });

    }

    private void cargarImagen() {

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(intent, PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Mostrando imagen desde galeria
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            imageUri = data.getData();
            imagePOKE.setImageURI(imageUri);
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                byte[] image = outputStream.toByteArray();
                String encodedString = Base64.encodeToString(image, Base64.DEFAULT);
                imagenString = encodedString;
                Log.d("codigo de galeria", encodedString);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}