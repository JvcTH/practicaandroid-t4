package com.example.examen_final;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.example.examen_final.databinding.ActivityMapsBinding;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ActivityMapsBinding binding;

    String lat;
    String lon;
    String poke;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMapsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        lat = getIntent().getStringExtra("lat");
        lon = getIntent().getStringExtra("lon");
        poke = getIntent().getStringExtra("poke");

        Log.e("lat ", lat);
        Log.e("lon ", lon);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera

        float latitude = Float.parseFloat(lat);
        float longitude = Float.parseFloat(lon);

        //ACERCAR CAMARA
        LatLng sydney2 = new LatLng(-7.1583243, -78.5191328);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney2, 14));

        LatLng sydney = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(sydney).title(poke));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
}