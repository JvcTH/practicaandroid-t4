package com.example.examen_final;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface servicio {


    @GET("pokemones/{ID}")
    Call<pokemon> getPoke(@Path("ID") String id);

    @GET("N00198603-3")
    Call<modelo> getEntrenador();

    @POST("N00198603-3")
    Call<Void> POST(@Body modelo entrenador);

    @POST("N00198603-3/crear")
    Call<Void> POSTcrear(@Body pokemon pok);

    @GET("N00198603-3")
    Call<List<pokemon>> getPokemones();

    @POST("entrenador/N00198603-3/pokemon")
    Call<pokemon> POSTcap(@Body pokemon cap);

    @GET("N00198603-3/pokemones")
    Call<List<pokemon>> getPokemonesCap();
}
