package com.example.examen_final;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class detallePokemonActivity extends AppCompatActivity {

    ImageView imagePOKE;
    EditText nombrePoke;
    EditText tipoPoke;
    String lat;
    String lon;
    String pokeN;

    Button verUbicaciones;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_pokemon);

        imagePOKE = findViewById(R.id.imagePOKE);
        nombrePoke = findViewById(R.id.nombrePoke);
        tipoPoke = findViewById(R.id.tipoPoke);
        verUbicaciones = findViewById(R.id.verUbicaciones);
        Button capturarPokemon = findViewById(R.id.capturarPokemon);

        String ID = getIntent().getStringExtra("Poke");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://upn.lumenes.tk/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        servicio service = retrofit.create(servicio.class);


        Call<pokemon> getOpke = service.getPoke(ID);
        getOpke.enqueue(new Callback<pokemon>() {
            @Override
            public void onResponse(Call<pokemon> call, Response<pokemon> response) {
                pokemon pokemon = response.body();

                String im = "https://upn.lumenes.tk" + pokemon.getUrl_imagen();
                Picasso.get()
                        .load(im)
                        .into(imagePOKE);
                nombrePoke.setText(pokemon.getNombre());
                pokeN = pokemon.getNombre();
                tipoPoke.setText(pokemon.getTipo());
                pokemon.setId(pokemon.getId());

                lat = String.valueOf(pokemon.getLatitude());
                lon = String.valueOf(pokemon.getLongitude());

            }

            @Override
            public void onFailure(Call<pokemon> call, Throwable t) {

            }
        });

        capturarPokemon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Retrofit POSpo = new Retrofit.Builder()
                        .baseUrl("https://upn.lumenes.tk/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                servicio pok = POSpo.create(servicio.class);

                pokemon pokemon = new pokemon();
                pokemon.setPokemon_id(ID);

                Call<pokemon> capP = pok.POSTcap(pokemon);
                capP.enqueue(new Callback<pokemon>() {
                    @Override
                    public void onResponse(Call<pokemon> call, Response<pokemon> response) {
                        String respuesta = String.valueOf(response.code());
                        if (respuesta.equals("200")) {
                            Toast.makeText(getApplicationContext(), "Pokemon Capturado", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "se movio el pokemon :'v", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<pokemon> call, Throwable t) {

                    }
                });
            }
        });

        verUbicaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                intent.putExtra("lat", lat);
                intent.putExtra("lon", lon);
                intent.putExtra("poke", pokeN);
                startActivity(intent);
            }
        });
    }
}
